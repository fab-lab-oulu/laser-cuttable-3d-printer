# Laser-cuttable 3D printer

We have started to work on a fully laser-cuttable 3D printer frame, which can be reproduced easily and cheaply. Motherboard is designed to be fabbable in any Fab Lab around the world.

The printer will be delta type with all the mechanisms made with laser cuttable methods.

First idea about the guide rails with gimbal heads is presented in the image

![](images/First_idea_19042021.jpg)
